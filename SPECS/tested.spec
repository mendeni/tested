# --
# -- spec file for tested
# --

# -- don't strip binary par files, only compress
%define __os_install_post /usr/lib/rpm/brp-compress

Summary: A utility for testing.
Name: tested
Version: 1.00
Release: 1
License: GPL
Group: System Environment/Tools
Source: %{name}-%{version}.tgz

URL: http://mendeni.com/tested.html
Vendor: Mendeni, LLC
Packager: swilson@mendeni.com

Requires: which
BuildRequires: which
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root

%description
This is a test package.

%package admin
Group: System Environment/Tools
Summary: Admin tools for use with tested.
Requires: tested

%description admin
This is a test.

%prep
rm -r -f -v %{buildroot}
%setup

%install
./configure

mkdir -p -v %{buildroot}%{_sbindir}
mkdir -p -v %{buildroot}%{_mandir}/man5

%{__install} -p -m0755 bin/tested %{buildroot}%{_sbindir}
%{__install} -p -m0755 bin/tested-encrypt %{buildroot}%{_sbindir}
%{__install} -p -m0755 bin/tested-decrypt %{buildroot}%{_sbindir}
%{__install} -p -m0644 bin/tested.man %{buildroot}%{_mandir}/man5/tested.5

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root)
%{_sbindir}/tested
%doc %{_mandir}/man5/tested.5.gz

%files admin
%defattr(-, root, root)
%{_sbindir}/tested-encrypt
%{_sbindir}/tested-decrypt

%changelog
* Wed Jul 31 2019 Steven Wilson <swilson@gitlab.com>
- New release 1.00
