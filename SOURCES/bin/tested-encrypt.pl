#!/usr/bin/perl

# -- our package name
package Tested;

# -- strict, warnings
use strict;
use warnings;

# -- required packages
use Crypt::Simple;

# -- turn off buffering
$|=1;

# -- define variables
my $password;
my $password2;

# -- turn off local echo
system('stty -echo');

if ($ENV{PASSWORD}){
  $password = $ENV{PASSWORD}    
}
else {
    while (! $password){
    
      # -- prompt for password
      print "Enter the password:\n";
      chomp($password = <STDIN>);
    
      next unless $password;
    
      # -- verify passwd
      while (! $password2){
    
        # -- prompt for password
        print "Enter the password again:\n";
        chomp($password2 = <STDIN>);
    
      }
      if ($password ne $password2){
        print "Passwords don't match - please try again\n";
        $password  = undef;
        $password2 = undef;
      }
    }
}

print encrypt($password),"\n";

# -- turn on local echo
system('stty echo');
