#!/usr/bin/perl

die "usage: decrypt X0OqHb8cDoabMdQYp71MC2Del2rdm89+frdn/jq4q7zryhUZeZb7jGWQTXcMlMUz\n" unless @ARGV eq 1;

# -- our package name
package Tested;

# -- strict, warnings
use strict;
use warnings;

# -- required packages
use Crypt::Simple;

# -- turn off buffering
$|=1;

print decrypt($ARGV[0]),"\n";
