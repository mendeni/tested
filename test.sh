#!/bin/bash -e

# set known test values
export PASSWORD='Hello'
export PASSWORD_HASH='XeDsnQnbecAS+xJxxV6dL6Ci6WIGVPA5Q2bOKk1e/lXtDRHFtM11Xg=='

# run the tests in order
for t in `ls tests/*`
do
  echo "Running $t"
  bash -xe $t
done
