#!/bin/bash -xe

# install epel for perl-PAR-Packer below
yum -y install https://download-ib01.fedoraproject.org/pub/epel/7/x86_64/Packages/e/epel-release-7-11.noarch.rpm

yum -y install rpm-build gnupg make gcc which perl-XML-Parser perl-XML-LibXML perl-libwww-perl perl-CPAN perl-PAR-Packer perl-Module-Runtime perl-Module-Implementation perl-Test-Fatal perl-Params-Validate perl-YAML

# install (more) perl deps via cpan
export PERL_MM_USE_DEFAULT=1;
export PERL_MM_NONINTERACTIVE=1;
export AUTOMATED_TESTING=1;

# create a cpan config
cat <<EOF > /usr/share/perl5/CPAN/Config.pm
\$CPAN::Config = {
  'build_cache' => q[10],
  'build_dir' => q[/root/.cpan/build],
  'cache_metadata' => q[1],
  'cpan_home' => q[/root/.cpan],
  'ftp' => q[/usr/bin/ftp],
  'ftp_proxy' => q[],
  'getcwd' => q[cwd],
  'gpg' => q[/usr/bin/gpg],
  'gzip' => q[/bin/gzip],
  'histfile' => q[/root/.cpan/histfile],
  'histsize' => q[100],
  'http_proxy' => q[],
  'inactivity_timeout' => q[0],
  'index_expire' => q[1],
  'inhibit_startup_message' => q[0],
  'keep_source_where' => q[/root/.cpan/sources],
  'links' => q[],
  'make' => q[/usr/bin/make],
  'make_arg' => q[],
  'make_install_arg' => q[],
  'makepl_arg' => q[],
  'ncftp' => q[],
  'ncftpget' => q[],
  'no_proxy' => q[],
  'pager' => q[/usr/bin/less],
  'prerequisites_policy' => q[ask],
  'scan_cache' => q[atstart],
  'shell' => q[/bin/bash],
  'tar' => q[/bin/tar],
  'term_is_latin' => q[1],
  'unzip' => q[/usr/bin/unzip],
  'urllist' => [q[http://cpan.mirror.clemson.edu/], q[ftp://mirror.datapipe.net/pub/CPAN/], q[ftp://ftp.wayne.edu/CPAN/]],
  'wget' => q[/usr/bin/wget],
};
1;
__END__
EOF

for p in Crypt::Simple Log::Dispatch::Syslog Crypt::Simple
do
  perl -MCPAN -e "install $p"
done

ln -s SOURCES tested-1.00
tar chvzpf tested-1.00.tgz tested-1.00/ 
mv tested-1.00.tgz SOURCES

# build
rpmbuild -ba --clean --define "_topdir `pwd`" --define "buildroot `pwd`/BUILDROOT" --define "_source_filedigest_algorithm md5" --define "_binary_filedigest_algorithm md5" --define "_source_payload nil" --define "_binary_payload nil" SPECS/*.spec
